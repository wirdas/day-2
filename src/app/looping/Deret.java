package app.looping;

public class Deret 
{
	public void listGanjil(int deret) 
	{
		if(deret > 0) 
		{
			System.out.print("Deret Bilangan Ganjil : ");
			
			for(int i=1; i<=deret; i++) 
			{
				if(i%2!=0) 
				{
					System.out.print(i + " ");
				}
			}
			
			System.out.print("\n");
		}
	}
}
