package app.mahasiswa.dao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;

import app.mahasiswa.ByClass;
import app.mahasiswa.Mahasiswa;

public class DAOByKelas 
{
	public void viewedKelas() 
	{
		Scanner j = new Scanner(System.in);
		Scanner in = new Scanner(System.in);
		List<ByClass> ls = new ArrayList<>();
	
		System.out.print("Input Jumlah Data :");
		int jml = j.nextInt();
		System.out.println("=============================================");
		if(jml > 0) 
		{
			for(int i = 1; i <= jml; i++) 
			{
				System.out.print("Input Name :");
				String name = in.nextLine();
				System.out.print("Input NPM :");
				String npm = in.nextLine();
				System.out.print("Input Telp :");
				String telp = in.nextLine();
				System.out.print("Input Address :");
				String address = in.nextLine();
				System.out.print("Input Kelas :");
				String kelas = in.nextLine();
				
				ByClass mhs = new ByClass();
				mhs.setName(name);
				mhs.setNpm(npm);
				mhs.setTelp(telp);
				mhs.setAddress(address);
				mhs.setKelas(kelas);
				ls.add(mhs);
				System.out.println("\n");
			}
		}
		
		System.out.print("Press any key to continue for showing data. . . ");
		in.nextLine();
		
		if(ls.size() > 0) 
		{
			int i = 1;
			
			for (ByClass kls : ls) 
			{
				System.out.println("=============================================");
				System.out.println("Data Ke - " + i);
				System.out.println("Name : " + kls.getName());
				System.out.println("NPM : " + kls.getNpm());
				System.out.println("Telp : " + kls.getTelp());
				System.out.println("Address : " + kls.getAddress());
				System.out.println("Kelas : " + kls.getKelas());
				i++;
			}
		}
	}
	
	public void viewedKelasMap() 
	{
		Scanner j = new Scanner(System.in);
		Scanner in = new Scanner(System.in);
		Map<Integer, ByClass> lsMap = new HashMap<>();
		
		System.out.print("Input Jumlah Data :");
		int jml = j.nextInt();
		System.out.println("=============================================");
		if(jml > 0) 
		{
			for(int i = 1; i <= jml; i++) 
			{
				System.out.print("Input Name :");
				String name = in.nextLine();
				System.out.print("Input NPM :");
				String npm = in.nextLine();
				System.out.print("Input Telp :");
				String telp = in.nextLine();
				System.out.print("Input Address :");
				String address = in.nextLine();
				System.out.print("Input Kelas :");
				String kelas = in.nextLine();
				
				ByClass mhs = new ByClass();
				mhs.setName(name);
				mhs.setNpm(npm);
				mhs.setTelp(telp);
				mhs.setAddress(address);
				mhs.setKelas(kelas);
				lsMap.put(i, mhs);
				System.out.println("\n");
			}
		}
		
		System.out.print("Cari data by index : ");
		int index = in.nextInt();
		
		if(lsMap.containsKey(index)) 
		{
			System.out.println("Data Ke - " + index);
			System.out.println("Name : " + lsMap.get(index).getName());
			System.out.println("NPM : " + lsMap.get(index).getNpm());
			System.out.println("Telp : " + lsMap.get(index).getTelp());
			System.out.println("Address : " + lsMap.get(index).getAddress());
			System.out.println("Kelas : " + lsMap.get(index).getKelas());
		}
		else 
		{
			System.out.println("Data tidak ditemukan dengan index " + index);
		}
	}
}
