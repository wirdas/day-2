package app.mahasiswa.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import app.mahasiswa.Mahasiswa;

public class DAOMahasiswa 
{
	List<Mahasiswa> ls = new ArrayList<>();
	Mahasiswa mhs = new Mahasiswa();
	
	public DAOMahasiswa() 
	{
		super();

		mhs.setName("Ayu Tingting");
		mhs.setNpm("1234");
		mhs.setAlamat("Alamat palsu");
		ls.add(mhs);
		
		mhs = new Mahasiswa();
		mhs.setName("Nelly");
		mhs.setNpm("2345");
		mhs.setAlamat("Cakung");
		ls.add(mhs);
	}

	public void getSingleMahasiswa() 
	{
		System.out.print(ls.get(0).getName() + "\n");
	}
	
	public void getListMahasiswa() 
	{
		for (Mahasiswa mahasiswa : ls) 
		{
			System.out.print(mahasiswa.getName() + " - " + mahasiswa.getNpm() + " - " + mahasiswa.getAlamat() + "\n");
		}
	}
	
	public void mapArray() 
	{
		Map<Integer, Mahasiswa> map = new HashMap<>();
		
		Mahasiswa obj = new Mahasiswa();
		obj.setName("Saya tidak tau");
		obj.setNpm("1234");
		obj.setAlamat("Alamat palsu");
		map.put(1, obj);
		
		for (Entry<Integer, Mahasiswa> entry:map.entrySet()) 
		{
//			Mahasiswa data = entry.getValue();
//			System.out.println("Name : " + data.getName());
			
			System.out.println("Name : " + entry.getValue().getName());
			System.out.println("NPM : " + entry.getValue().getNpm());
			System.out.println("ALamat : " + entry.getValue().getAlamat());
		}
		System.out.println("\n");
	}
}
