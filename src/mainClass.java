import java.io.IOException;

import app.looping.Deret;
import app.mahasiswa.dao.DAOByKelas;
import app.mahasiswa.dao.DAOMahasiswa;

public class mainClass 
{
	public static void main(String[] args) throws IOException 
	{
		Deret cDeret = new Deret();
		DAOMahasiswa daoMhs = new DAOMahasiswa();
		DAOByKelas daoKelas = new DAOByKelas();
	
//		System.out.print("Hello world \n");
//		
//		System.out.println("----------------------------- Deret Bilangan --------------------------------\n");
//		cDeret.listGanjil(10);
//		
//		System.out.println("----------------------------- Array List --------------------------------\n");
//		daoMhs.getSingleMahasiswa();
//		daoMhs.getListMahasiswa();
//		
//		System.out.println("----------------------------- MAP --------------------------------\n");
//		daoMhs.mapArray();
		
		System.out.println("----------------------------- View Kelas --------------------------------\n");
		//daoKelas.viewedKelas();
		daoKelas.viewedKelasMap();
	}
}